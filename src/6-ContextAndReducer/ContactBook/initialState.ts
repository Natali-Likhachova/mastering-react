import { v4 as uuidv4 } from 'uuid';
import * as faker from 'faker';

import { ContactsState } from './types';

export const initialState: ContactsState = {
    contacts: [
        {
            id: uuidv4(),
            name: 'Tiger',
            phone: faker.phone.phoneNumberFormat(),
        },
        {
            id: uuidv4(),
            name: 'Sam',
            phone: faker.phone.phoneNumberFormat(),
        },
    ],
    selectedId: null,
};
