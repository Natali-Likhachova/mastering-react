## Project: Mastering React

### Prerequisites
Before getting started, ensure you have the following tools installed:

- Git - Version control system
- VS Code - Integrated Development Environment (IDE) / Code Editor (Not mandatory to use VS Code, but all configuration examples will be shown using VS Code)
- Node.js - JavaScript runtime for executing JavaScript scripts outside the browser

### How to Run the Project Locally
I recommend using one of the following package managers:

- npm - Default package manager that comes bundled with Node.js.
- yarn - In this course, yarn will be used. ❗️ Note: I will be using version 1 specifically, and I recommend you do the same. Versions 2 and 3 have a completely different architecture, and you may encounter difficulties installing certain libraries.

There are other package managers available (e.g., pnpm), but I do not recommend them for beginners.

| Step          | npm Command         | yarn Command      |
|---------------|---------------------|-------------------|
| Install Dependencies | npm install      | yarn              |
| Start Development Server | npm run dev   | yarn dev          |
| Build for Production | npm run build   | yarn build        |
| Preview Production Version | npm run preview | yarn preview    |

❗️ Most importantly, choose only one package manager and stick with it.

❗️ NPM creates its package-json.lock file, while Yarn creates its yarn.lock. If you install dependencies alternately using different managers, you may encounter subtle bugs.