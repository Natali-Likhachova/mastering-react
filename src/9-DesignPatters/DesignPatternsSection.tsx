import { Chapter, Section } from 'components/MasterDetail';
import { RouterPath } from 'config/RouterPath';

import { HooksFactory } from './HooksFactory/HooksFactory';
import { EmojiNumberedListExample } from './CompoundComponent/EmojiNumberedListExample';
import { ToggleButtonExample } from './CompoundComponent/ToggleButtonExample';

export function DesignPatternsSection(): JSX.Element {
    return (
        <Section title="Design patterns">
            <Chapter
                emoji="🏭"
                title="Hooks factory"
                path={RouterPath.HOOKS_FACTORY}
                element={<HooksFactory />}
            />
            <Section title="Compound component">
                <Chapter
                    emoji="🔢"
                    title="Emoji numbered list"
                    path={RouterPath.EMOJI_NUMBERED_LIST}
                    element={<EmojiNumberedListExample />}
                />
                <Chapter
                    emoji="🎛"
                    title="Toggle button"
                    path={RouterPath.TOGGLE_BUTTON}
                    element={<ToggleButtonExample />}
                />
            </Section>
        </Section>
    );
}
